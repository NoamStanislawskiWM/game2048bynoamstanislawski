package edu.wm.cs.cs301.game2048;
import java.util.Arrays;
import java.util.Random;

public class State implements GameState {
	
	int SIDE_LEN = 4;
	int[] valueBoard; // this is the 1d array
	
	
	public State() {
		this.valueBoard = new int[16];
	}

	public State(State state) {
		this.valueBoard = state.valueBoard.clone();
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(valueBoard);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		State other = (State) obj;
		if (!Arrays.equals(valueBoard, other.valueBoard))
			return false;
		return true;
	}

	@Override
	public int getValue(int x, int y) {
		int index = x + (SIDE_LEN * y); // formula converting 2d --> 1d
		return valueBoard[index];
	}

	@Override
	public void setValue(int x, int y, int value) {
		int index = x + (SIDE_LEN * y);
		valueBoard[index] = value;

	}

	@Override
	public void setEmptyBoard() {
		valueBoard = new int[16]; //reinitializing fills with zeros
	}

	@Override
	public boolean addTile() {
		boolean tileFound = false;
		Random rand = new Random();
		

		if(!isFull()) {
			while(!tileFound) {
				int randIndex =	rand.nextInt(16);
				if(valueBoard[randIndex] == 0) { // if the random spot is 0, place a 2 or 4
					valueBoard[randIndex] = rand.nextBoolean() ? 2 : 4; // chooses between 2 or 4
					tileFound = true;
				}
			}
			return true;
		}
		
		else {
			return false;
		}			
	}

	@Override
	public boolean isFull() {
		boolean result = true;
		for(int i = 0; i < valueBoard.length; i++) { // Check if board is full or not
			if(valueBoard[i] == 0) {
				result = false;
			}
		}
		return result;
	}

	@Override
	public boolean canMerge() {
		/*
		 * Checks if there is at least one merge by copying the board to a dummy state and running all movements upon it.
		 * If any score is made, meaning a merge is possible, then it return true. Otherwise, it will be false.
		 */
		State testState = new State(this);  
		
		if (testState.left() == 0 && testState.right() == 0 && testState.up() == 0 && testState.right() == 0) {
			return false;
		}
		else {
			return true;
		}
	}

	@Override
	public boolean reachedThreshold() {
		/*
		 * Check each space to see if there is a 2048 tile.
		 */
		boolean result = false;
		for(int i = 0; i < valueBoard.length;i++) {
			if(valueBoard[i] >= 2048) {
				result = true;
			}
		}
	return result;
	}

	@Override
	public int left() { 
		/*
		 * The left method works by shifting all tiles left, checking for possible horizontal merges,
		 *  and then shifting once again to account for possible empty spaces. This same ideology is used
		 *  in every movement method, though the directions change based upon choice.
		 *  The score is taken from the newly merged tile.
		 *  
		 *  The variable "i" is used in these methods for iterating through to shift.
		 *  The variable "cur" is used for the iteration of merging tiles. 
		 */
		
		int score = 0;
		
		for(int i = 1; i < valueBoard.length; i++) {
			leftShift(i);
		}
		
		for(int cur = 15; cur >= 0; cur--) { // Checking and merging "current" tile
			if(cur % 4 < 3) { //check if tile to the left is not on the far right
				if(valueBoard[cur+1] == valueBoard[cur]) { // tile to the right is equal to current tile
					valueBoard[cur] *= 2;
					valueBoard[cur+1] = 0;
					score += valueBoard[cur];
				}
			}
		}
		
		for(int i = 1; i < valueBoard.length; i++) { //reshift tiles back once taken into account newly created tiles 
			leftShift(i);
		}
		return score;
	}
	
	private int leftShift(int index) {
		/*
		 * This is a recursive method, as all of my directionShift methods are.
		 * The process of every shift method is similar, but changes slightly based on direction.
		 * It first checks if the index gives an edge tile, which is a base case. If it not at an edge it
		 * either switches to the edge or induces recursion to continually switch until the edge is reached.
		 * Right, up, and down are less complex/convuluted in their creation, but this left method has been finicky. 
		 */
		
		if(valueBoard[index] != 0 && index % 4 != 0) {
			if(index-1 % 4 == 0 && valueBoard[index-1] == 0) { //index is in second column and shifting left
				valueBoard[index-1] = valueBoard[index];
				valueBoard[index] = 0;
				return index;
			}
			else if(index-1 % 4 != 0 && valueBoard[index-1] == 0) {
				valueBoard[index-1] = valueBoard[index];
				valueBoard[index] = 0;
				leftShift(index-1);
				
			}
			else {
				return index;
			}
		}
		return index;
	}
	
	@Override
	public int right() {
		int score = 0;
		for(int i = 14; i >= 0; i-- ) {
			rightShift(i);
		}
		
		for(int cur = 15; cur >= 0; cur--) { // Checking and merging "current" tile
			if(cur % 4 != 0) {
				if(valueBoard[cur-1] == valueBoard[cur]) {
					valueBoard[cur] *= 2;
					valueBoard[cur-1] = 0;
					score += valueBoard[cur];
				}
			}
		}
		for(int i = 14; i >= 0; i-- ) {
			rightShift(i);
		}
		return score;
	}
	
	private int rightShift(int index) { // recursive version
		if(index % 4 != 3) {
			if(valueBoard[index] != 0 && valueBoard[index+1] == 0) {
				valueBoard[index+1] = valueBoard[index];
				valueBoard[index] = 0;
				rightShift(index+1);
			}
		}

		return index;	
	}
	
	@Override
	public int down() {
		int score = 0;
		
		for(int i = 15; i >= 0; i--) {
			downShift(i);
		}
		
		for(int cur = 0; cur < 12; cur++) {
			if(valueBoard[cur+4] == valueBoard[cur]) {
				valueBoard[cur+4] *= 2;
				valueBoard[cur] = 0;
				score += valueBoard[cur+4];
			}
		}
		for(int i = 15; i >= 0; i--) {
			downShift(i);
		}
		return score;
	}
	
	private int downShift(int index) {
		if(index+4 < 16) { //are you not at the top row
			if(valueBoard[index+4] == 0 && valueBoard[index] != 0) {
				valueBoard[index+4] = valueBoard[index];
				valueBoard[index] = 0;
				downShift(index+4);
			}
		}
		return index;
	}
	
	@Override
	public int up() {
		int score = 0;
		
		for(int i = 0; i < valueBoard.length; i++) {
			upShift(i);
		}
		for(int cur = 0; cur < 12; cur++) {
			if(valueBoard[cur+4] == valueBoard[cur]) {
				valueBoard[cur] *= 2;
				valueBoard[cur+4] = 0;
				score += valueBoard[cur];
			}
		}
		for(int i = 0; i < valueBoard.length; i++) {
			upShift(i);
		}
		
		return score;
	}
	
	private int upShift(int index) {
		if(index-4 >= 0) { //are you not at the top row
			if(valueBoard[index-4] == 0 && valueBoard[index] != 0) {
				valueBoard[index-4] = valueBoard[index];
				valueBoard[index] = 0;
				upShift(index-4);
			}
		}
		return index;
	}

}
